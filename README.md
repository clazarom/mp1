MP1
===

Conversion of:
-Temperature
-Weight
-Distance.

Recommended extras:
-Keyboard dismissal gesture (by tapping outside), 
-Automatic conversion direction selection 
-"Insta" conversions 
-Localization (Spanish language).
